/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * cercle.c
 */
#include <stdio.h>


int main() {
    float rayon = 5;
    
    float aire = rayon * rayon * 3.14;

    float perimetre = 2 * 3.14 * rayon;

    printf("Rayon : %f cm, Aire : %f cm2, Perimetre : %f cm\n", rayon, aire, perimetre);


    return 0;
}