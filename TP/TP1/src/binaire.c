/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * binaire.c
 */
#include <stdio.h>

int main(){
    unsigned int decimal;

    printf("Entre un entier à convertir: ");
    scanf("%d", &decimal);
    unsigned int n = decimal;

    // Définir le nombre de bits pour écrire le nombre
    int bits = 0;
    while(n){
        bits++;
        // Décale les bits de l'integer sur la droite jusqu'à arriver à 0
        n >>= 1;
    }
    int binary[bits];
    unsigned int value;

  for (int i = 0; decimal > 0; i++){
      value = decimal % 2;
      decimal = decimal / 2;
      binary[i] = value;
  }

  for(int i = bits-1; i >= 0; i--){
      // Tableau lu à l'envers pour placer correctement le LSB et le MSB
      // On ne lit pas l'élement à l'emplacement 17 car on lit celui à l'emplacement 0
      printf("%d ", binary[i]);
  }
    printf("\n");
    return 0;
}