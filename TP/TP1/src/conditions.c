/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * conditions.c
 */
#include <stdio.h>

int main()
{
    int i = 0;
    int match = 0;
    // On entre dans la boucle
    while(i <= 1000){
        match = 0;
        // On affiche les numéros correspondants à une des conditions
        if (i % 2 == 0 && i % 15 == 0)
        {
            printf("Divisible par 2 et 15; ");
            match = 1;
        }
        if (i % 103 == 0 || i % 107 == 0)
        {
            printf("Divisible par 103 et 107; ");
            match = 1;
        }
        if ((i % 7 == 0 || i % 5 == 0) && i % 3 > 0)
        {
            printf("Divisible par 7 ou par 5 mais pas par 3; ");
            match = 1;
        }
        if (match){
            // Si le nombre correspond à une des conditions
            printf("%d\n\n", i);
        }

        // On ajoute 1 et on recommence jusqu'a 1000
        i++;
    }
    return 0;
}
