/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * ptrvariables.c
 */
#include <stdio.h>

int i = -3;
long int li = -123455678989;
long long int lli = -123434567678899;
short sh = -15454;

unsigned short ush = 0x1234;
unsigned int ui = 0x3;
unsigned long int uli = 0x0123456789abcdef;
unsigned long long int ulli = 0xAAFFFFFFFFFFFFBB;

char ch = 'b';
unsigned char uch = 'u';

float f = 0.333333;

double dou = 0.34456454566;
long double ldou = 0.34843084384398439843;

int main(){


    int* ptri = &i;
    long int* ptrli = &li;
    long long int* ptrlli = &lli;
    short* ptrsh = &sh;

    unsigned short* ptrush = &ush;
    unsigned int* ptrui = &ui;
    unsigned long int* ptruli = &uli;
    unsigned long long int* ptrulli = &ulli;

    char* ptrch = &ch;
    unsigned char* ptruch = &uch;

    float* ptrf = &f;

    double* ptrdou = &dou;
    long double* ptrldou = &ldou;
    printf("Addr: %p - Int: %d\n", ptri, *ptri);
    printf("Addr: %p - Long Int: %ld\n", ptrli, *ptrli);
    printf("Addr: %p - Long Long Int: %lld\n", ptrlli, *ptrlli);
    printf("Addr: %p - Short: %d\n", ptrsh, *ptrsh);

/*    unsigned char* toto = (unsigned char *) ptrush;
    for (int i = 0; i < 128; i++){
        printf("%i - %x\n", i, *(toto+i));
    }*/
    printf("Addr: %p - Unsigned Short: %d\n", ptrush, *ptrush);
    printf("Addr: %p - Unsigned Int: %u\n", ptrui, *ptrui);
    printf("Addr: %p - Unsigned Long Int: %lu\n", ptruli, *ptruli);
    printf("Addr: %p - Unsigned Long Int: %lu\n", ptruli, *ptruli);

    printf("Addr: %p - Unsigned Long Long Int: %llu\n", ptrulli, *ptrulli);

    printf("Addr: %p - Char: %c\n", ptrch, *ptrch);
    printf("Addr: %p - Unsigned Char: %c\n", ptruch, *ptruch);

    printf("Addr: %p - Float: %f\n", ptrf, *ptrf);
    printf("Addr: %p - Double: %g\n", ptrdou, *ptrdou);
    printf("Addr: %p - Long Double: %Lg\n", ptrldou, *ptrldou);
    return 0;
}