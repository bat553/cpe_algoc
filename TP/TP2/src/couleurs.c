/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * couleurs.c
*/
#include <stdio.h>

int main(){
    int n = 0;
    typedef unsigned char byte;
    typedef unsigned int RGBA; // 0xRRGGBBAA
    typedef struct { //
        byte alpha;
        byte bleu;
        byte vert;
        byte rouge;
    } Tcouleur;


    RGBA couleurs[2] = {
            0xffffaabb, // R=FF;G=FF;B=AA;A=BB
            0xffaabbcc
    };

    Tcouleur* toto;

    for(int i = 0; i < 2; i++){
        ptrcouleur = (Tcouleur*) &couleurs[i]; // Transtype

        printf("R=0x%x ", ptrcouleur->rouge);
        printf("G=0x%x ", ptrcouleur->vert);
        printf("B=0x%x ", ptrcouleur->bleu);
        printf("A=0x%x\n", ptrcouleur->alpha);
    }


    return 0;
}
