/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * etudiant2.c
*/
#include <stdio.h>
#include <string.h>

int main(){
    struct adresse {
        char rue[120];
        char ville[120];
    };
    struct etudiant {
        char nom[120];
        char prenom[120];
        struct adresse adresse;
        char notes_algo[9];
        char notes_sys[9];
    };

    struct etudiant etudiants[5];

    strcpy(etudiants[0].prenom, "Jane");
    strcpy(etudiants[0].nom, "Tall");
    strcpy(etudiants[0].adresse.rue, "155 Goodland Crossing");
    strcpy(etudiants[0].adresse.ville, "Luohe");
    strcpy(etudiants[0].notes_sys, "12 15 9");
    strcpy(etudiants[0].notes_algo, "19 5 8");

    strcpy(etudiants[1].prenom, "Christoffer");
    strcpy(etudiants[1].nom, "Mullet");
    strcpy(etudiants[1].adresse.rue, "43 Streensland Plaza");
    strcpy(etudiants[1].adresse.ville, "Ravne");
    strcpy(etudiants[1].notes_sys, "8 18 10");
    strcpy(etudiants[1].notes_algo, "16 8 10");

    strcpy(etudiants[2].prenom,"Phil");
    strcpy(etudiants[2].nom, "Howells");
    strcpy(etudiants[2].adresse.rue, "1 Anthes Point");
    strcpy(etudiants[2].adresse.ville, "Flandes");
    strcpy(etudiants[2].notes_sys, "12 14 15");
    strcpy(etudiants[2].notes_algo, "18 10 10");

    strcpy(etudiants[3].prenom,  "Sherilyn");
    strcpy(etudiants[3].nom, "Le Friec");
    strcpy(etudiants[3].adresse.rue,  "41908 Scofield Terrace");
    strcpy(etudiants[3].adresse.ville,  "Lamoso");
    strcpy(etudiants[3].notes_sys, "17 12 11");
    strcpy(etudiants[3].notes_algo, "16 17 11");

    strcpy(etudiants[4].prenom,"Maxwell");
    strcpy(etudiants[4].nom, "Roskeilly");
    strcpy(etudiants[4].adresse.rue, "501 Rowland Place");
    strcpy(etudiants[4].adresse.ville, "Olivia");
    strcpy(etudiants[4].notes_sys, "14 12 10");
    strcpy(etudiants[4].notes_algo, "4 5 10");


    // affichage
    for (int i = 0; i < 5; i++){
        printf("Prenom: %s\nNom: %s\nRue: %s\nVille: %s\nNotes Algo: %s\nNotes Sys: %s\n",
               etudiants[i].prenom, etudiants[i].nom, etudiants[i].adresse.rue,
               etudiants[i].adresse.ville, etudiants[i].notes_algo, etudiants[i].notes_sys);
        printf("________\n");
    }


    return 0;
}