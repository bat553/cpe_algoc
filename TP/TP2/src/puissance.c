/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * puissance.c
 */
#include <stdio.h>

int main(){
    int b = 8; // Exponent
    int a = 2;
    int result = 1;
    for(int i = 0; i < b; i++){
        result = result * a;
    }

    printf("%d\n", result);


    return 0;
}