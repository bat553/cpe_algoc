/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * bits.c
 */

#include <stdio.h>

int main(){
    long long int d;

    printf("Entre un entier à convertir: ");
    scanf("%lld", &d);

    if (d & 0x100010){
        printf("%d\n", 1);
    } else {
        printf("%d\n", 0);
    }

    return 0;
}