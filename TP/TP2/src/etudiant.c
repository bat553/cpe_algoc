#include <stdio.h>
#include <string.h>

int main(){

    char prenom[5][30] = {
            "Jane",
            "Patrick",
            "Christoffer",
            "Phil",
            "Sherilyn"
    };
    char nom[5][30] = {
            "Tall",
            "Le Roublard",
            "Mullet",
            "Howells",
            "Roskeilly"
    };
    char rue[5][120] = {
            "155 Goodland Crossing",
            "96 Avenue Achille Peretti, 92200 Neuilly-sur-Seine",
            "43 Streensland Plaza",
            "1 Anthes Point",
            "501 Rowland Place",
    };
    char ville[5][120] = {
            "Luohe",
            "Neuilly-sur-Seine",
            "Ravne",
            "Flandes",
            "Olivia",
    };
    char notes_algo[5][30] = {
            "12 15 9",
            "12 15 19",
            "8 18 10",
            "12 14 15",
            "14 12 10",
    };
    char notes_sys[5][30] = {
            "19 5 8",
            "14 15 8",
            "16 8 10",
            "18 10 10",
            "4 5 10"
    };

    // affichage
    for (int i = 0; i < 5; i++){
            printf("Prenom: %s\nNom: %s\nRue: %s\nVille: %s\nNotes Algo: %s\nNotes Sys: %s\n",
                   prenom[i], nom[i], rue[i], ville[i], notes_algo[i], notes_sys[i]);
        printf("________\n");
    }


    return 0;
}