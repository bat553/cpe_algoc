/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * fibonacci.c
*/

#include <stdio.h>

int main(){
    unsigned int n = 0;
    unsigned int Un;
    unsigned int U1 = 0;
    unsigned int U2 = 1;
    printf("n termes de la suite de Fibonacci:");
    scanf("%d", &n);


    for(int i = 0; i <= n; i++){
        // Terme en cours
        Un = U1 + U2;
        if (i == 0){
            Un = 0;
        } else if (i % 2 == 0){
            // Si nombre paire Un-1 = Un; Sinon Un-2 = Un
            U1 = Un;
        } else {
            U2 = Un;
        }
        printf("U%d = %d\n", i, Un);
    }

    return 0;
}