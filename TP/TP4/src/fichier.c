/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * fichier.c
*/

#include <stdio.h>

void lire_fichier(char *nom_de_fichier){
    char c[10000]; // Buffer
    FILE *ptr_fichier;
    if((ptr_fichier = fopen(nom_de_fichier, "r")) == NULL){ // Mode lecture
        printf("Erreur pendant l'ouverture du fichier!");
    }
    fscanf(ptr_fichier, "%[^\n]", c); // Ajouter tous les caractères sauf les retours charriot
    printf("%s\n", c);

    fclose(ptr_fichier);
}

void ecrire_dans_fichier(char* nom_de_fichier, char *message){
    FILE *ptr_fichier;
    if((ptr_fichier = fopen(nom_de_fichier, "a")) == NULL){ // Mode écriture/lecture
        printf("Erreur pendant l'ouverture du fichier!");
    }
    fputs(message, ptr_fichier); // Ajouter le message dans le fichier
    fclose(ptr_fichier); // Fermer le fichier
}