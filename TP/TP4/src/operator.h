/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * operator.h
*/
//déclaration des fonctions
int somme (int, int);
int difference (int, int);
int produit (int, int);
int quotient (int, int);
int modulo (int, int);
int et (int, int);
int ou (int, int);
int negation (int, int);