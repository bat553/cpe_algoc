/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * etudiant_bd.c
*/
#include <stdio.h>
#include <stdbool.h>

bool lire_ligne(char *chaine, size_t max) //fonction pour utiliser scanf
{
    size_t i;

    for (i = 0; i < max - 1; ++i)
    {
        int c;

        if (scanf("%lc", &c) != 1) //Récupère chaque caractère saisie par l'utilisateur
            return false;
        else if (c == '\n')
            break;

        chaine[i] = c; //enregistre le caractere dans un tableau de char
    }

    chaine[i] = '\0'; //ajoute le dernier caractère
    return true;
}
void ecrire_dans_fichier(char* nom_de_fichier, char *message){ //fonction pour écrire dans un fichier
    FILE *ptr_fichier;
    if((ptr_fichier = fopen(nom_de_fichier, "a")) == NULL){ // Mode écriture/lecture
        printf("Erreur pendant l'ouverture du fichier!");
    }
    fputs(message, ptr_fichier); // Ajouter le message dans le fichier
    fclose(ptr_fichier); // Fermer le fichier
}
int main(){
    struct adresse { //définition de la structure adresse
        char rue[120];
        char ville[120];
    };
    struct etudiant { //définition de la structure etudiant
        char nom[120];
        char prenom[120];
        struct adresse adresse;
        char notes_algo[10];
        char notes_sys[10];
    };
    char virgule[2]=","; //Définition variable caractère virgule
    char saut[2]="\n"; //Définiton variable caractère saut de ligne
    struct etudiant etudiants[5];

        
    for (int i = 0; i < 5; i++){ //Boucle pour : Qui demande les infos de 5 étudiants et qui les écrit dans un fichier
          printf("prénom:\n"); //Affiche la demande du prenom
          if (lire_ligne(etudiants[i].prenom, sizeof etudiants[i].prenom)) //Appel de la fnction du saisit utilisateur
            ecrire_dans_fichier("texte.txt",etudiants[i].prenom);//ecrit le prenom dans le fichier
            ecrire_dans_fichier("texte.txt",virgule);//écrit une virgule à la suite
            
          printf("nom:\n");
          if (lire_ligne(etudiants[i].nom, sizeof etudiants[i].nom))//Appel de la fnction du saisit utilisateur
            ecrire_dans_fichier("texte.txt",etudiants[i].nom);//ecrit le nom dans le fichier
            ecrire_dans_fichier("texte.txt",virgule);//écrit une virgule à la suite

          printf("rue:\n");
          if (lire_ligne(etudiants[i].adresse.rue, sizeof etudiants[i].adresse.rue))//Appel de la fnction du saisit utilisateur
            ecrire_dans_fichier("texte.txt",etudiants[i].adresse.rue);//ecrit la rue dans le fichier
            ecrire_dans_fichier("texte.txt",virgule);//écrit une virgule à la suite

          printf("ville:\n");
          if (lire_ligne(etudiants[i].adresse.ville, sizeof etudiants[i].adresse.ville))//Appel de la fnction du saisit utilisateur
            ecrire_dans_fichier("texte.txt",etudiants[i].adresse.ville);//ecrit la ville dans le fichier
            ecrire_dans_fichier("texte.txt",virgule);//écrit une virgule à la suite

          printf("note algo:\n");
          if (lire_ligne(etudiants[i].notes_algo, sizeof etudiants[i].notes_algo))//Appel de la fnction du saisit utilisateur
            ecrire_dans_fichier("texte.txt",etudiants[i].notes_algo);//ecrit les notes dans le fichier
            ecrire_dans_fichier("texte.txt",virgule);//écrit une virgule à la suite

          printf("notes sys:\n");
          if (lire_ligne(etudiants[i].notes_sys, sizeof etudiants[i].notes_sys))//Appel de la fnction du saisit utilisateur
            ecrire_dans_fichier("texte.txt",etudiants[i].notes_sys);//ecrit les notes dans le fichier
            ecrire_dans_fichier("texte.txt",saut);//saute une ligne dans le fichier
    }
    return 0;

}


