/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * calcule.c
*/
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char **argv){
    int num1 = atoi(argv[2]); // Conversion de char vers int
    int num2 = atoi(argv[3]);
    int res;
    if(argc != 4){ // Vérification des entrées
        printf("Usage: %s <exp> <num1> <num2>\n", argv[0]);
        return 1;
    }

    switch (*argv[1]) { // Switch sur l'argument spécifié en CLI
        case '+':
            res = (num1 + num2);
            printf("%d\n", res);
            break;
        case '-':
            res = (num1 - num2);
            printf("%d\n",res); break;
        case '*':
            res = (num1 * num2);
            printf("%d\n",res); break;
        case '/':
            res = (num1 / num2);
            printf("%d\n",res); break;
        case '%':
            res = (num1 % num2);
            printf("%d\n",res); break;
        case '&':
            res = (num1 && num2);
            printf("%d\n",res); break;
        case '|':
            res = (num1 || num2);
            printf("%d\n",res); break;
        case '~':
            printf("%d %d\n",(~ num1),(~ num2)); break;

        default:
            printf("Mauvais opérateur\n"); return 1;

    }

    return 0;
}