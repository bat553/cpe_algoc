/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * factorielle.c
*/

#include <stdio.h>

int factorielle (int num){
    long int total = 1;
    if (num == 0){
        return 0;
    }
    for(int i = 1; i <= num; i++){
        // Multiplication du i jusqu'a i==num
        total = total * i;
    }

    return total;

}

int main(){
    int num = 5;
    printf("%d\n", factorielle(num));
    return 0;
}