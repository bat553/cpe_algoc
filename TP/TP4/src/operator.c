/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * operator.c
*/
#include <stdio.h>
//définition du contenu des fonction
int somme (int num1, int num2) {
  int num3 = num1 + num2;
  return num3;
}
int difference (int num1, int num2) {
  int num3 = num1 - num2;
  return num3;
}
int produit (int num1, int num2) {
  int num3 = num1 * num2;
  return num3;
}
int quotient (int num1, int num2) {
  int num3 = num1 / num2;
  return num3;
}
int modulo (int num1, int num2) {
  int num3 = num1 % num2;
  return num3;
}
int et (int num1, int num2) {
  int num3 = num1 && num2;
  return num3;
}
int ou (int num1, int num2) {
  int num3 = num1 || num2;
  return num3;
}
int negation (int num1, int num2) {
  int num3 = num1 && num2;
  return num3;
}

int main(){
    int num1 = 1;//définition de la variable num1
    int num2 = 10;//définition de la variable num2
    char op = '+';//définition de la variable op
    int res;//définition de la variable resultat

    switch (op) {//switch de l'opérateur en focntion de la variable op
        case '+':
            res = somme (num1,num2); //calcul
            printf("%d\n",res); break;//affiche le résultat
        case '-':
            res = difference (num1,num2);//calcul
            printf("%d\n",res); break;//affiche le résultat
        case '*':
            res = produit (num1,num2);
            printf("%d\n",res); break;//affiche le résultat
        case '/':
            res = quotient (num1,num2);
            printf("%d\n",res); break;//affiche le résultat
        case '%':
            res = modulo (num1,num2);
            printf("%d\n",res); break;//affiche le résultat
        case '&':
            res = et (num1,num2);
            printf("%d\n",res); break;//affiche le résultat
        case '|':
            res = ou (num1,num2);
            printf("%d\n",res); break;//affiche le résultat
        case '~':
            res = negation (num1,num2);
            printf("%d\n",res); break;//affiche le résultat

        default:
            printf("Mauvais opérateur\n"); return 1;

    }

    return 0;
}