/*
 * PELLARIN/CODA - 2020
 * https://gitlab.com/bat553/cpe_algoc
 * chercherfichier.c
*/

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv){

    if (argc != 3){
        printf("Usage: %s <recherche> <chemin fichier>\n", argv[0]);
        return 1;
    }

    char* chemin = argv[2];
    char* recherche = argv[1];
    int length = strlen(recherche); // Taille de la recherche
    char chaine[140]; // Taille arbitraire pour socker une chaine de caractère afin de vérifier son contenu
    FILE* fichier;

    int compte = 0; // Compte les occurrences du mot recherché dans le fichier

    fichier = fopen(chemin, "r"); // Ouverture en mode lecture uniquement

    // Lecture ligne par lignes d'un flux
    while(fgets(chaine, 140, fichier) != NULL){
        // Tant qu'il y a du contenu
        int j = 0;
        for(int i = 0; i < strlen(chaine); i++){
            //printf("%c %c\n", chaine[i], recherche[j]);
            if (chaine[i] == recherche[j]){ // Compare caractères par caractères
                j++;
            } else {
                j=0;
            }
            if(j == length){
                // Correspondance trouvée
                compte++;
            }
        }
    }
    printf("Mot(s) trouvé(s) %d\n", compte);

    return 0;
}