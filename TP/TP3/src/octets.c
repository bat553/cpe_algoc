/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * octets.c
*/

#include <stdio.h>

int main(){
    printf("%lu \n",sizeof (int *));
    printf("%lu \n",sizeof (int **));
    printf("%lu \n",sizeof (char *));
    printf("%lu \n",sizeof (char **));
    printf("%lu \n",sizeof (char ***));
    printf("%lu \n",sizeof (float *));
    printf("%lu \n",sizeof (float **));
    printf("%lu \n",sizeof (float ***));
    return 0;
}
