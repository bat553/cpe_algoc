/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * tri.c
*/

#include <stdio.h>
#include <stdlib.h>

int main(){

    int tableau_int[100];

    for(int i = 0; i < 100; i++){
        tableau_int[i] = rand()%200;
    }


    // bubble Sort
    int k = 0; // Sorted items
    int i = 0; // Index
    while (k < 100){ // Tant que tous les index ne seront pas triés
        if (i + 1 >= 100){
            // repartir à 0 quand on arrive à la fin
            i = 0;
        }
        if (tableau_int[i] > tableau_int[i + 1]){
            // Intervertir les deux index du tableau.
            int tempI = tableau_int[i];
            int tempI1 = tableau_int[i + 1];

            tableau_int[i] = tempI1;
            tableau_int[i + 1] = tempI;

            k = 0;
        } else {
            k++;
        }
        i++;

    }

    // Print
    for(int i = 0; i < 100; i++){
        printf("%d\n", tableau_int[i]);
    }

    return 0;
}