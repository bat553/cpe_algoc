/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * grand_petit.c
*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    int tableau_int[100];

    for(int i = 0; i < 100; i++){
        tableau_int[i] = rand()%200;
    }

    // Recherche le plus grand et le plus petit
    int plus_grand = 0;
    int plus_petit = 0;
    for(int i = 0; i < 100; i++){
        // Si l'entier est plus grand que celui actuellement stoqué
        if (tableau_int[i] > plus_grand){
            plus_grand = tableau_int[i];
        }
        // Si l'entier est plus petit que celui stocké ou si celui stocké est égal à 0
        if (tableau_int[i] < plus_petit || plus_petit == 0){
            plus_petit = tableau_int[i];
        }
    }

    printf("Plus grand : %d\nPlus petit : %d\n", plus_grand, plus_petit);

    return 0;
}