/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * recherche_dichotomique.c
*/

#include <stdio.h>


int main(){
    int tableau_int_trie[100];
    for(int i = 0; i < 100; i++){
        tableau_int_trie[i] = i+1;
    }

    int recherche;
    // On demande quel entier est recherché
    printf("Quel entier recherchez vous?: ");
    scanf("%d", &recherche);

    // Binary Search
    int n;
    int start = 0;
    int stop = 99;
    while(start <= stop){
        // On va sur l'entier du milieu
        n = (start + stop) / 2;

        /* Si il est plus grand on recherche sur la partie plus grande
         * ou sur la partie plus petite si l'entier est plus petit.
        */

        if(tableau_int_trie[n] < recherche){
            start = n + 1;
        } else if(tableau_int_trie[n] > recherche){
            stop = n - 1;
        } else {
            break;
        }
    }
    if(tableau_int_trie[n] == recherche){
        printf("Entier présent\n");
    } else {
        printf("Entier Introuvable.\n");
    }




    return 0;
}