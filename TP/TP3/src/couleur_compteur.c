/*
 * Baptiste PELLARIN - 2020
 * <baptiste.pellarin@cpe.fr> - https://gitlab.com/bat553/cpe_algoc
 * couleur_compteur.c
*/
#include <stdio.h>
#include <stdlib.h>

int main(){
    int n = 0;
    struct Couleur { //
        int alpha;
        int bleu;
        int vert;
        int rouge;
        int nb_occurence;
        int doublon_deja_detecte;
    };
    struct Compare {
        struct Couleur c;
        int nb;
    };



    struct Couleur couleurs[100];

    for(int i = 0; i < 100; i++){
        couleurs[i].rouge = rand()%256;
        couleurs[i].vert = rand()%256;
        couleurs[i].bleu = rand()%256;
        couleurs[i].alpha = rand()%256;
        couleurs[i].doublon_deja_detecte = 0;
        couleurs[i].nb_occurence = 1;
    }

    // Deux couleurs identiques pour le test
    couleurs[0].rouge = 12;
    couleurs[0].vert = 12;
    couleurs[0].bleu = 12;
    couleurs[0].alpha = 12;

    couleurs[1].rouge = 12;
    couleurs[1].vert = 12;
    couleurs[1].bleu = 12;
    couleurs[1].alpha = 12;

    couleurs[10].rouge = 13;
    couleurs[10].vert = 12;
    couleurs[10].bleu = 12;
    couleurs[10].alpha = 12;

    couleurs[11].rouge = 14;
    couleurs[11].vert = 12;
    couleurs[11].bleu = 12;
    couleurs[11].alpha = 12;

    couleurs[20].rouge = 12;
    couleurs[20].vert = 12;
    couleurs[20].bleu = 12;
    couleurs[20].alpha = 12;

    couleurs[21].rouge = 13;
    couleurs[21].vert = 12;
    couleurs[21].bleu = 12;
    couleurs[21].alpha = 12;

    for (int i = 0; i < 100; i++) {
        // Skip si couleur déjà comptée
        if (couleurs[i].doublon_deja_detecte == 0) {
            for (int j = i + 1; j < 100; j++) {
                if (couleurs[i].rouge == couleurs[j].rouge &&
                    couleurs[i].vert == couleurs[j].vert &&
                    couleurs[i].bleu == couleurs[j].bleu &&
                    couleurs[i].alpha == couleurs[j].alpha) {

                    // Si il y a un doublon, incrémenter les occurences et marqué comme compté.
                    couleurs[i].nb_occurence++;
                    couleurs[j].doublon_deja_detecte = 1;

                }
            }
            // Afficher la couleur et passer à la suivante
            printf("R=0x%x ", couleurs[i].rouge);
            printf("G=0x%x ", couleurs[i].vert);
            printf("B=0x%x ", couleurs[i].bleu);
            printf("A=0x%x ", couleurs[i].alpha);
            printf("Count=%d\n", couleurs[i].nb_occurence);
        }
    }

    return 0;
}